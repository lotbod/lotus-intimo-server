namespace Server.Entities.Shop
{
    public class StockData
    {
        public string Size { get; set; }
        public string Color { get; set; }
        public string Code { get; set; }
        public float Price { get; set; }
        public int Available { get; set; } = 1;
    }
}