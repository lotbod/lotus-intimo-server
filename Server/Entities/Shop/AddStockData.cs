using Microsoft.AspNetCore.Mvc;

namespace Server.Entities.Shop
{
    [ModelBinder(BinderType = typeof(MetadataValueModelBinder))]
    public class AddStockData
    {
        public string Size { get; set; }
        public string Color { get; set; }
        public string Code { get; set; }
        public string Price { get; set; } = "0";
    }
}