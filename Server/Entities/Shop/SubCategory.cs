namespace Server.Entities.Shop
{
    public enum SubCategory
    {
        Bra = 0,
        UnderwearSet = 1,
        Pants = 2,
        Body = 3,
        Tights = 4,
        ForPregnantUnderwear = 5,
        PantsSet = 6,
        Tops = 7,
        Tshirts = 8,
        Socks = 9,
        Leggins = 10,
        Swimsuits = 11,
        SwimPants = 12,
        Termo = 13
    }
}