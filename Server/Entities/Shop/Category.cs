namespace Server.Entities.Shop
{
    public enum Category
    {
        Woman = 0,
        Man = 1,
        Kid = 2,
        Certificates = 3
    }
}