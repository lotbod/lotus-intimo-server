using System.Collections.Generic;

namespace Server.Entities.Shop
{
    public class ShopItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public float Price { get; set; } 
        public string Brand { get; set; } 
        public string Description { get; set; }
        public Category Category { get; set; } 
        public SubCategory SubCategory { get; set; }
        public int Discount { get; set; } //
        public IEnumerable<StockData> StockData { get; set; }
        public bool Novelty { get; set; }
        public bool Heat { get; set; }
        public IEnumerable<string> Images { get; set; }
        public string Composition { get; set; }
    }
}