using System;
using System.Collections.Generic;

namespace Server.Entities.Shop
{
    [Serializable]
    public class ShopDto
    {
        public IEnumerable<ShopItemDto> Items;
    }
}