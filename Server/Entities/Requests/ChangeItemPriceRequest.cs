namespace Server.Entities.Requests
{
    public class ChangeItemPriceRequest : IRequest
    {
        public int Id { get; set; }
        public double Price { get; set; }
    }
}