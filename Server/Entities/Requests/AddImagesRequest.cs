using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Server.Entities.Requests
{
    public class AddImagesRequest : IRequest
    {
        public int Id { get; set; }
        public List<IFormFile> Images { get; set; }
    }
}