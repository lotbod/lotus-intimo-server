namespace Server.Entities.Requests
{
    public class ChangeItemStringRequest : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}