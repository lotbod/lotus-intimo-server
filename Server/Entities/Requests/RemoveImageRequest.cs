namespace Server.Entities.Requests
{
    public class RemoveImageRequest : IRequest
    {
        public string Id { get; set; }
        public string ImageKey { get; set; }    
    }
}