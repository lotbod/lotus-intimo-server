using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Server.Entities.Shop;

namespace Server.Entities.Requests
{
    public class AddShopItemRequest : IRequest
    {
        public string Name { get; set; }
        public string Brand { get; set; }
        public string Description { get; set; }
        
        public Category Category { get; set; }
        public SubCategory SubCategory { get; set; }
        public float Price { get; set; }
        public int Discount { get; set; }

        public List<AddStockData> StockData { get; set; }
        
        public bool Novelty { get; set; }
        public bool Heat { get; set; }
        
        public List<IFormFile> Images { get; set; }
        public string Composition { get; set; }
    }
}