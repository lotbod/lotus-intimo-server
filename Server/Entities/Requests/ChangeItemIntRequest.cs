namespace Server.Entities.Requests
{
    public class ChangeItemIntRequest : IRequest
    {
        public int Id { get; set; }
        public int Value { get; set; }
    }
}