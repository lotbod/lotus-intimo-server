using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Server.Entities.Requests
{
    public class OrderRequest : IRequest
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public DeliveryType DeliveryType { get; set; }
        public PaymentType PaymentType { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string Department { get; set; }
        public string ShopAddress { get; set; }
        public List<OrderItem> Items { get; set; }
    }

    [ModelBinder(BinderType = typeof(MetadataValueModelBinder))]
    public class OrderItem
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public int Discount { get; set; }
        public float Price { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }

        public override string ToString()
        {
            return $"<p>   *   {Count}шт. {Name} ({Code}) Колір: {Color}, Розмір: {Size}, Вартість за 1 шт: {Price - Price * Discount / 100f}грн, " +
                   (Discount > 0 ? $"Знижку {Discount}% враховано." : "Знижки немає.</p>");
        }
    }
}