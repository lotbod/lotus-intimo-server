namespace Server.Entities.Requests
{
    public class VerifyUserRequest : IRequest
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; } = null;
        public string FacebookToken { get; set; } = null;
    }
}