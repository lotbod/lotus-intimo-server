using Server.Entities.Shop;

namespace Server.Entities.Requests
{
    public class AddToUserCartRequest : IRequest
    {
        public string Email { get; set; }
        public ShopItemDto ShopItem { get; set; }
    }
}