using System.Collections.Generic;
using Server.Entities.Shop;
using Server.Entities.Users;

namespace Server.Entities.Requests
{
    public class SetUserCartRequest : IRequest
    {
        public string Email { get; set; }
        public UserCart Cart { get; set; }
    }
}