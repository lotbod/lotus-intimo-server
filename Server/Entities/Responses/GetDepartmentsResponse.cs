using System.Collections.Generic;
using Server.Controllers.Data;

namespace Server.Entities.Responses
{
    public class GetDepartmentsResponse : ServerResponse
    {
        public IEnumerable<DepartmentsResponseData.DepartmentInfo> Departments { get; set; }
    }
}