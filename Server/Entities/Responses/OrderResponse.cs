namespace Server.Entities.Responses
{
    public class OrderResponse : IResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}