using Server.Entities.Users;

namespace Server.Entities.Responses
{
    public class GetUserCartResponse : IResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public UserCart Cart { get; set; } 
    }
}