using Microsoft.AspNetCore.Mvc;

namespace Server.Entities.Responses
{
    public class GetImageResponse : ServerResponse
    {
        public FileResult Image { get; set; }
    }
}