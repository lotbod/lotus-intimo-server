using System.Collections.Generic;

namespace Server.Entities.Responses
{
    public class UpdateImagesResponse : ServerResponse
    {
        public List<string> ImageKeys { get; set; } = new List<string>();
    }
}