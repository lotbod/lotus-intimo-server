using System.Collections.Generic;

namespace Server.Entities.Responses
{
    public class RemoveImageResponse : ServerResponse
    {
        public List<string> AllKeys { get; set; } = new List<string>();
    }
}