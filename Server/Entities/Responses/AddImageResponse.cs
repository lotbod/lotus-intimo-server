using System.Collections.Generic;

namespace Server.Entities.Responses
{
    public class AddImageResponse : ServerResponse
    {
        public List<string> ImagesKey { get; set; } = new List<string>();
        public List<string> AllKeys { get; set; } = new List<string>();
    }
}