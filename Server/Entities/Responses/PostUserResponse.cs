namespace Server.Entities.Responses
{
    public class PostUserResponse : IResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}