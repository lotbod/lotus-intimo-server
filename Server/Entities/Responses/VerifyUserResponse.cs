using Server.Entities.Users;

namespace Server.Entities.Responses
{
    public class VerifyUserResponse : ServerResponse
    {
        public string Secret { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; } = string.Empty;
        public UserCart Cart { get; set; } = new UserCart();
    }
}