namespace Server.Entities.Responses
{
    public class ServerResponse : IResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}