using System.Collections.Generic;
using Server.Controllers.Api;
using Server.Controllers.Data;

namespace Server.Entities.Responses
{
    public class GetAreasResponse : ServerResponse
    {
        public IEnumerable<AreasResponseData.AreaInfo> Areas { get; set; }
    }
}