namespace Server.Entities.Responses
{
    public interface IResponse
    {
        bool Success { get; set; }
        public string ErrorMessage{ get; set; }

    }
}