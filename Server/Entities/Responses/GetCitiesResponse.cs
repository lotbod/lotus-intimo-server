using Server.Controllers.Api;
using System.Collections.Generic;
using Server.Controllers.Data;

namespace Server.Entities.Responses
{
    public class GetCitiesResponse : ServerResponse
    {
        public IEnumerable<CitiesResponseData.CityInfo> Cities { get; set; }
    }
}