namespace Server.Entities
{
    public enum DeliveryType
    {
        SelfPickup = 0,
        NovaPoshta = 1
    }
}