namespace Server.Entities.Users
{
    public class UserDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string FacebookToken { get; set; } = string.Empty;
    }
}