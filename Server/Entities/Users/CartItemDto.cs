namespace Server.Entities.Users
{
    public class CartItemDto
    {
        public int Count { get; set; }
        public int Id { get; set; }
    }
}