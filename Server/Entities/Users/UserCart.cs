using System.Collections.Generic;

namespace Server.Entities.Users
{
    public class UserCart
    {
        public IEnumerable<CartItemDto> Items { get; set; }
    }
}