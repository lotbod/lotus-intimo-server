namespace Server.Entities.Users
{
    public class UserCreds
    {
        
        public string Password { get; set; } = string.Empty;
        public string FacebookToken { get; set; } = string.Empty;
    }
}