namespace Server.Entities
{
    public enum PaymentType
    {
        Cash = 0,
        Card = 1
    }
}