using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace Server.Logging
{
    public class ServerLogger : IServerLogger
    {
        private readonly StringBuilder _stringBuilder = new StringBuilder();
        private readonly string _logFilePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "/server/logs.txt";

        public ServerLogger()
        {
            if (File.Exists(_logFilePath))
            {
                File.Delete(_logFilePath);
            }

            File.Create(_logFilePath).Dispose();
        }
        
        public void Log(object obj)
        {
            Log(JsonConvert.SerializeObject(obj));
        }

        public void Log(string text)
        {
            _stringBuilder.Append(DateTime.Now.ToShortDateString());
            _stringBuilder.Append(' ');
            _stringBuilder.Append(DateTime.Now.ToLongTimeString());
            _stringBuilder.Append("| ");
            _stringBuilder.Append(text);
            
            using(var writer = File.AppendText(_logFilePath))
            {
                writer.WriteLine(_stringBuilder.ToString());
                writer.WriteLine();
            }
            
            _stringBuilder.Clear();
        }

        public string[] GetLogTrace()
        {
            return File.ReadAllLines(_logFilePath);
        }
    }
}