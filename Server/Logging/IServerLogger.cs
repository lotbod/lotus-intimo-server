using System;

namespace Server.Logging
{
    public interface IServerLogger
    {
        public void Log(object obj);
        public void Log(string text);
        public string[] GetLogTrace();
    }
}