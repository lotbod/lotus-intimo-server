using Server.Entities.Requests;

namespace Server.Mailing
{
    public interface IMailHandler
    {
        public void SendMessage(OrderRequest order);
    }
}