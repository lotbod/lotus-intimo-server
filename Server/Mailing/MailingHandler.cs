using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using Newtonsoft.Json;
using Server.Data;
using Server.Entities;
using Server.Entities.Requests;

namespace Server.Mailing
{
    public class MailingHandler : IMailHandler
    {
        private const string Empty = "Відсутнє";
        private StringBuilder _stringBuilder = new StringBuilder();

        public void SendMessage(OrderRequest order)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(Configs.Email);
            message.Subject = "Нове Замовлення";
            message.To.Add(new MailAddress(Configs.Email));
            message.IsBodyHtml = true;

            _stringBuilder.Append("<html><body>");
            _stringBuilder.Append($"<p>Ім'я: {order.Name}</p>");
            _stringBuilder.Append($"<p>Прізвище: {CheckForEmpty(order.Surname)}</p>");
            _stringBuilder.Append($"<p>Номер телефону: {order.PhoneNumber}</p>");
            _stringBuilder.Append($"<p>E-Mail: {CheckForEmpty(order.Email)}</p>");
            _stringBuilder.Append($"<p>Метод Оплати: {PaymentTypeToStr(order.PaymentType)}</p>");
            _stringBuilder.Append($"<p>Метод доставки: {DeliveryTypeToStr(order.DeliveryType)}</p>");

            if (order.DeliveryType == DeliveryType.SelfPickup)
            {
                _stringBuilder.Append($"<p>Магазин: {order.ShopAddress}</p>");
            }
            else if (order.DeliveryType == DeliveryType.NovaPoshta)
            {
                _stringBuilder.Append($"<p>Область: {order.Area}</p>");
                _stringBuilder.Append($"<p>Місто: {order.City}</p>");
                _stringBuilder.Append($"<p>Відділення: {order.Department}</p>");
            }

            _stringBuilder.Append("<p>Товари:</p>");

            float totalPrice = 0f;

            foreach (var orderItem in order.Items)
            {
                _stringBuilder.Append(orderItem);
                totalPrice += orderItem.Price - orderItem.Price * orderItem.Discount / 100f;
            }

            _stringBuilder.Append("<p></p>");
            _stringBuilder.Append($"<p>Загальна ціна: {totalPrice}грн.");
            _stringBuilder.Append("</body></html>");

            message.Body = _stringBuilder.ToString();

            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential(Configs.Email, Configs.WindowsEmailKey),
                EnableSsl = true
            };

            smtpClient.Send(message);
            _stringBuilder.Clear();
        }

        private string CheckForEmpty(string text) => string.IsNullOrEmpty(text) ? Empty : text;

        private string DeliveryTypeToStr(DeliveryType type)
        {
            switch (type)
            {
                case DeliveryType.SelfPickup:
                    return "Самовивіз";
                case DeliveryType.NovaPoshta:
                    return "Нова Пошта";
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private string PaymentTypeToStr(PaymentType type)
        {
            switch (type)
            {
                case PaymentType.Cash:
                    return "Готівка";
                case PaymentType.Card:
                    return "Карта";
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}