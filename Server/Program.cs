﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Server.Logging;
using Server.Mailing;

namespace Server
{
    public class Program
    {
        public static readonly IServerLogger ServerLogger = new ServerLogger();
        public static readonly IMailHandler MailHandler = new MailingHandler();
        
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls("https://0.0.0.0:5001");
                });
    }
}

