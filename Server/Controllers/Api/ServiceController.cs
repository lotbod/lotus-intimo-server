using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Server.Entities.Requests;
using Server.Entities.Responses;

namespace Server.Controllers.Api
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ServiceController
    {
        private readonly ILogger<ServiceController> _logger;

        public ServiceController(ILogger<ServiceController> logger)
        {
            _logger = logger;
        }
                
        [HttpPost("MakeOrder")]
        public Task<OrderResponse> MakeOrder([FromForm] OrderRequest request)
        {
            Program.ServerLogger.Log($"HttpPost | Service/MakeOrder");
            Program.ServerLogger.Log(request);
            
            _logger.Log(LogLevel.Information, JsonConvert.SerializeObject(request));

            var response = new OrderResponse();

            if (string.IsNullOrEmpty(request.Name) || string.IsNullOrEmpty(request.PhoneNumber) 
                && request.Items.Count <= 0)
            {
                response.Success = false;
                response.ErrorMessage = "RequestEmptyValuesError";

                return Task.FromResult(response);
            }

            try
            {
                Program.MailHandler.SendMessage(request);

                response.Success = true;
                response.ErrorMessage = "OrderComplete";
            }
            catch (Exception _)
            {
                response.Success = false;
                response.ErrorMessage = "MailNotSentError";
                return Task.FromResult(response);
            }

            
            return Task.FromResult(response);
        }
        
        [HttpGet("GetLogs")]
        public Task<string[]> GetLogs()
        {
            return Task.FromResult(Program.ServerLogger.GetLogTrace());
        }
    }
}