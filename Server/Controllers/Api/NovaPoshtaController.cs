using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Server.Controllers.Data;
using Server.Data;
using Server.Entities.Responses;

namespace Server.Controllers.Api
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NovaPoshtaController
    {
        private readonly HttpClient _httpClient = new HttpClient();
        private readonly ILogger<NovaPoshtaController> _logger;
        
        public NovaPoshtaController(ILogger<NovaPoshtaController> logger)
        {
            _logger = logger;
            
            if(!NovaPoshtaDatabase.CitiesResponseData.success)
                GetNovaPoshtaCities();
        }

        private async void GetNovaPoshtaCities()
        {
            var request = new ApiRequest
            {
                apiKey = Configs.NovaPoshtaApiKey,
                modelName = "Address",
                calledMethod = "getCities"
            };

            var data = await _httpClient.PostAsync(new Uri(Configs.NovaPoshtaApi),
                new StringContent(JsonConvert.SerializeObject(request)));
            
            var json = await data.Content.ReadAsStringAsync();
            
            Console.WriteLine(json);

            NovaPoshtaDatabase.CitiesResponseData = JsonConvert.DeserializeObject<CitiesResponseData>(json);
            
            Console.WriteLine(NovaPoshtaDatabase.CitiesResponseData.success);
            Console.WriteLine(NovaPoshtaDatabase.CitiesResponseData.data.Length);
            
            _logger.Log(LogLevel.Debug, $"Cities loaded: {NovaPoshtaDatabase.CitiesResponseData?.data.Length}");
        }

        [HttpGet("GetAreas")]
        public async Task<GetAreasResponse> GetAreas()
        {
            Program.ServerLogger.Log($"HttpGet | NovaPoshta/GetAreas");
            
            var response = new GetAreasResponse();

            var request = new ApiRequest
            {
                apiKey = Configs.NovaPoshtaApiKey,
                modelName = "Address",
                calledMethod = "getAreas"
            };

            var data = await _httpClient.PostAsync(new Uri(Configs.NovaPoshtaApi),
                new StringContent(JsonConvert.SerializeObject(request)));

            if (data.IsSuccessStatusCode)
            {
                var json = await data.Content.ReadAsStringAsync();
                var responseData = JsonConvert.DeserializeObject<AreasResponseData>(json);

                if (responseData != null)
                {
                    response.Success = true;
                    response.Areas = responseData.data;
                }
            }
            else
            {
                response.Success = false;
                response.ErrorMessage = "UnknownError";
            }

            return response;
        }

        [HttpGet("GetCities")]
        public Task<GetCitiesResponse> GetCities(string areaRef)
        {
            Program.ServerLogger.Log($"HttpGet | NovaPoshta/GetCities");
            Program.ServerLogger.Log($"areaRef: {areaRef}");
            
            var response = new GetCitiesResponse();
            var cities = NovaPoshtaDatabase.CitiesResponseData.data.Where(x => x.Area == areaRef).ToArray();

            response.Success = true;
            response.Cities = cities;

            return Task.FromResult(response);
        }

        [HttpGet("GetDepartments")]
        public async Task<GetDepartmentsResponse> GetDepartments(string cityRef)
        {
            Program.ServerLogger.Log($"HttpGet | NovaPoshta/GetDepartments");
            Program.ServerLogger.Log($"cityRef: {cityRef}");
            
            var response = new GetDepartmentsResponse();

            var request = new ApiRequest
            {
                apiKey = Configs.NovaPoshtaApiKey,
                modelName = "Address",
                calledMethod = "getWarehouses",
                methodProperties = new ApiRequest.Props
                {
                    CityRef = cityRef
                }
            };

            var data = await _httpClient.PostAsync(new Uri(Configs.NovaPoshtaApi),
                new StringContent(JsonConvert.SerializeObject(request)));
            
            if (data.IsSuccessStatusCode)
            {
                var json = await data.Content.ReadAsStringAsync();
                var responseData = JsonConvert.DeserializeObject<DepartmentsResponseData>(json);

                if (responseData != null)
                {
                    response.Success = true;
                    response.Departments = responseData.data;
                }
            }
            else
            {
                response.Success = false;
                response.ErrorMessage = "UnknownError";
            }

            return response;
        }

        [Serializable]
        private class ApiRequest
        {
            public string apiKey;
            public string modelName;
            public string calledMethod;
            public Props methodProperties;
            
            [Serializable]
            public class Props
            {
                public string CityRef { get; set; }
            }
        }
    }
}