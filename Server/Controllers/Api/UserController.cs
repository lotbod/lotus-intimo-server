using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Server.Data;
using Server.Entities.Requests;
using Server.Entities.Responses;
using Server.Entities.Users;

namespace Server.Controllers.Api
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;

        public UserController(ILogger<UserController> logger)
        {
            _logger = logger;
        }

        [HttpGet("GetAdminContent")]
        public GetAdminContentResponse GetAdminContent(string secret)
        {
            var response = new GetAdminContentResponse
                {Success = !string.IsNullOrEmpty(secret) && secret.Equals(Configs.Secret)};

            return response;
        }

        [HttpGet("GetCart")]
        public async Task<GetUserCartResponse> GetUserCart(string email)
        {
            var response = new GetUserCartResponse();

            if (string.IsNullOrEmpty(email))
            {
                return response;
            }

            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();
                
                var dataAdapter = new MySqlDataAdapter($"SELECT * FROM Users WHERE Email = '{email}'",
                    connection);
                var dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                if (dataTable.Rows.Count != 0)
                {
                    var cartString = (string) dataTable.Rows[0].ItemArray[7];
                    response.Cart = !string.IsNullOrEmpty(cartString)
                        ? JsonConvert.DeserializeObject<UserCart>(cartString)
                        : new UserCart {Items = new List<CartItemDto>()};
                    response.Success = true;
                }
            }

            return response;
        }

        [HttpPost("Verify")]
        public async Task<VerifyUserResponse> VerifyUser([FromBody] VerifyUserRequest request)
        {
            var response = new VerifyUserResponse();

            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();

                var dataAdapter = new MySqlDataAdapter($"SELECT * FROM Users WHERE Email = '{request.Email}'",
                    connection);
                var dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                if (dataTable.Rows.Count > 0)
                {
                    var userData = dataTable.Rows[0].ItemArray;

                    bool passwordCondition = !string.IsNullOrEmpty(request.Password) &&
                                             request.Password.Equals((string) userData[2]);

                    bool fbTokenCondition = !string.IsNullOrEmpty(request.FacebookToken) &&
                                            request.FacebookToken.Equals((string) userData[3]);

                    if (passwordCondition || fbTokenCondition)
                    {
                        response.Success = true;
                        response.Secret = (bool) userData[1] ? Configs.Secret : string.Empty;

                        response.Name = (string) dataTable.Rows[0].ItemArray[4];
                        response.Email = (string) dataTable.Rows[0].ItemArray[5];
                        response.Phone = (string) dataTable.Rows[0].ItemArray[6];

                        var cartString = (string) dataTable.Rows[0].ItemArray[7];
                        response.Cart = !string.IsNullOrEmpty(cartString)
                            ? JsonConvert.DeserializeObject<UserCart>(cartString)
                            : new UserCart {Items = new List<CartItemDto>()};
                    }
                }
                else if (!string.IsNullOrEmpty(request.Email) && !string.IsNullOrEmpty(request.FacebookToken))
                {
                    var values = $"('{request.FacebookToken}', '{request.Name}', '{request.Email}')";
                    var command = new MySqlCommand("INSERT INTO Users(FacebookToken, Name, Email) VALUES" + values,
                        connection);
                    var resultCommand = command.ExecuteNonQuery();

                    response.Success = resultCommand == 1;
                    response.Name = request.Name;
                    response.Email = request.Email;
                }
            }

            return response;
        }

        [HttpPost("Register")]
        public async Task<PostUserResponse> RegisterUser([FromBody] UserDto dto)
        {
            var response = new PostUserResponse();

            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();
                
                var dataAdapter = new MySqlDataAdapter($"SELECT * FROM Users WHERE Email = '{dto.Email}'",
                    connection);
                var dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                if (dataTable.Rows.Count == 0)
                {
                    var values =
                        $"('{dto.Password}', '{dto.FacebookToken}', '{dto.Name}', '{dto.Email}', '{dto.Phone}')";
                    var command = new MySqlCommand(
                        "INSERT INTO Users(Password, FacebookToken, Name, Email, Phone) VALUES" + values,
                        connection);
                    var resultCommand = command.ExecuteNonQuery();

                    response.Success = resultCommand == 1;
                }
                else
                {
                    response.Success = false;
                }
            }

            return response;
        }

        [HttpPost("SetCart")]
        public async Task<SetUserCartResponse> SetUserCart([FromBody] SetUserCartRequest request)
        {
            var response = new SetUserCartResponse();

            if (string.IsNullOrEmpty(request.Email) || request.Cart?.Items == null)
            {
                response.ErrorMessage = "V tebe nulli v zapyti.";
                return response;
            }

            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();
                
                var dataAdapter = new MySqlDataAdapter($"SELECT * FROM Users WHERE Email = '{request.Email}'",
                    connection);
                var dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                if (dataTable.Rows.Count != 0)
                {
                    var cartString = JsonConvert.SerializeObject(request.Cart);

                    var command = new MySqlCommand(
                        "UPDATE Users SET Cart = '" + cartString + "' WHERE Email = '" + request.Email + "'",
                        connection);
                    var resultCommand = command.ExecuteNonQuery();

                    response.Success = resultCommand == 1;
                }
                else
                {
                    response.ErrorMessage = $"No such user as '{request.Email}'";
                }
            }

            return response;
        }

        [HttpDelete("Delete")]
        public async Task<bool> Delete(int id)
        {
            bool result;
            
            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();
                var command = new MySqlCommand("DELETE FROM Users WHERE Id = " + id, connection);
                result = command.ExecuteNonQuery() == 1;
            }

            return result;
        }
    }
}