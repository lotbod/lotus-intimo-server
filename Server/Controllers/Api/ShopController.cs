using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Server.Data;
using Server.Entities.Requests;
using Server.Entities.Responses;
using Server.Entities.Shop;

namespace Server.Controllers.Api
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopController : ControllerBase
    {
        private readonly ILogger<ShopController> _logger;

        public ShopController(ILogger<ShopController> logger)
        {
            _logger = logger;
        }

        [HttpGet("Test")]
        public string TestGet()
        {
            return "TEST WORKS!";
        }

        [HttpGet("GetItems")]
        public async Task<IEnumerable<ShopItemDto>> Get()
        {
            Program.ServerLogger.Log($"HttpGet | Shop/GetItems");
            
            List<ShopItemDto> shopItems = new List<ShopItemDto>();

            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();

                var dataAdapter = new MySqlDataAdapter("SELECT * FROM ShopItems", connection);
                var dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];

                    var dataString = (string)row.ItemArray[8];
                    var imagesString = (string)row.ItemArray[11];

                    var stockData = !string.IsNullOrEmpty(dataString)
                        ? JsonConvert.DeserializeObject<IEnumerable<StockData>>(dataString)
                        : new List<StockData>();

                    var images = !string.IsNullOrEmpty(imagesString)
                        ? JsonConvert.DeserializeObject<IEnumerable<string>>(imagesString)
                        : new List<string>();

                    shopItems.Add(new ShopItemDto
                    {
                        Id = (int)row.ItemArray[0],
                        Name = (string)row.ItemArray[1],
                        Price = (float)row.ItemArray[2],
                        Brand = (string)row.ItemArray[3],
                        Description = (string)row.ItemArray[4],
                        Category = (Category)row.ItemArray[5],
                        SubCategory = (SubCategory)row.ItemArray[6],
                        Discount = (int)row.ItemArray[7],
                        StockData = stockData,
                        Novelty = (bool)row.ItemArray[9],
                        Heat = (bool)row.ItemArray[10],
                        Images = images,
                        Composition = (string)row.ItemArray[12]
                    });
                }
            }

            return shopItems;
        }

        [HttpGet("GetImage")]
        public async Task<IActionResult> GetImage(string imageKey)
        {
            Program.ServerLogger.Log($"HttpGet | Shop/GetImage");
            Program.ServerLogger.Log($"imageKey: {imageKey}");
            
            string imagesFolderPath =
                Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "/server/images/";
            string filePath = imagesFolderPath + imageKey + ".jpeg";

            if (System.IO.File.Exists(filePath))
            {
                var imageBytes = await System.IO.File.ReadAllBytesAsync(filePath);
                
                return File(imageBytes, "image/jpeg");
            }

            return NotFound("Not found");
        }

        [HttpPost("UpdateImages")]
        public async Task<IActionResult> UpdateImages([FromForm] UpdateImagesRequest request)
        {
            Program.ServerLogger.Log($"HttpPost | Shop/UpdateImages");
            Program.ServerLogger.Log(request);
            
            var response = new UpdateImagesResponse();

            if (request.Images.Count == 0)
                return Ok(response);

            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();

                var dataAdapter = new MySqlDataAdapter("SELECT * FROM ShopItems WHERE Id = " + request.Id,
                    connection);
                var dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                if (dataTable.Rows.Count > 0)
                {
                    var imagesFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                                           "/server/images";

                    var imagesString = (string)dataTable.Rows[0].ItemArray[11];

                    if (!string.IsNullOrEmpty(imagesString))
                    {
                        var imagesPaths = JsonConvert.DeserializeObject<List<string>>(imagesString);

                        for (int i = 0; i < imagesPaths.Count; i++)
                        {
                            var imagePath = $"{imagesFolderPath}/{imagesPaths[i]}.jpeg";

                            if (System.IO.File.Exists(imagePath))
                            {
                                System.IO.File.Delete(imagePath);
                            }
                        }
                    }

                    foreach (var file in request.Images)
                    {
                        if (file.Length > 0)
                        {
                            var imageKey = Guid.NewGuid().ToString();

                            await using (var fileStream =
                                         new FileStream(Path.Combine(imagesFolderPath, imageKey + ".jpeg"),
                                             FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                            }

                            response.ImageKeys.Add(imageKey);
                        }
                    }

                    var command = new MySqlCommand(
                        $"UPDATE ShopItems SET Images='{JsonConvert.SerializeObject(response.ImageKeys)}' WHERE Id = {request.Id}",
                        connection);

                    response.Success = command.ExecuteNonQuery() == 1;
                }
            }

            return Ok(response);
        }

        [HttpPost("AddImages")]
        public async Task<IActionResult> AddImages([FromForm] AddImagesRequest request)
        {
            Program.ServerLogger.Log($"HttpPost | Shop/AddImages");
            Program.ServerLogger.Log(request);
            
            var response = new AddImageResponse();

            if (ReferenceEquals(request.Images, null))
                return Ok(response);

            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();

                var dataAdapter = new MySqlDataAdapter("SELECT * FROM ShopItems WHERE Id = " + request.Id, connection);
                var dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                if (dataTable.Rows.Count > 0)
                {
                    var imagesFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                                           "/server/images";

                    var imagesString = (string)dataTable.Rows[0].ItemArray[11];

                    if (!string.IsNullOrEmpty(imagesString))
                    {
                        response.AllKeys = JsonConvert.DeserializeObject<List<string>>(imagesString);
                    }

                    foreach (var file in request.Images)
                    {
                        if (file.Length > 0)
                        {
                            var imageKey = Guid.NewGuid().ToString();

                            await using (var fileStream =
                                         new FileStream(Path.Combine(imagesFolderPath, imageKey + ".jpeg"),
                                             FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                            }

                            response.ImagesKey.Add(imageKey);
                            response.AllKeys.Add(imageKey);
                        }
                    }
                }

                var command = new MySqlCommand(
                    $"UPDATE ShopItems SET Images='{JsonConvert.SerializeObject(response.AllKeys)}' WHERE Id = {request.Id}",
                    connection);

                response.Success = command.ExecuteNonQuery() == 1;
            }

            return Ok(response);
        }

        [HttpPost("AddItem")]
        public async Task<ShopResponse> AddItem([FromForm] AddShopItemRequest request)
        {
            Program.ServerLogger.Log($"HttpPost | Shop/AddItem");
            Program.ServerLogger.Log(request);
            
            var response = new ShopResponse();
            foreach (var data in request.StockData)
            {
                Console.WriteLine(data.Price);
            }

            if (string.IsNullOrEmpty(request.Name) || request.Price <= 0 || request.StockData == null
                || request.StockData.Count == 0
                || request.Images == null || request.Images.Count == 0)
            {
                response.ErrorMessage = "Wrong data";
                return response;
            }

            var stockDataList = request.StockData.Select(element => new StockData
            {
                Size = element.Size,
                Color = element.Color,
                Code = element.Code,
                Price = element.Price == "0" || !float.TryParse(element.Price, out var parsed) ? request.Price : parsed,
                Available = 1
            });

            var stockData = JsonConvert.SerializeObject(stockDataList);
            var imageKeys = new List<string>();

            var imagesFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                                   "/server/images";
            foreach (var file in request.Images)
            {
                if (file.Length > 0)
                {
                    var imageKey = Guid.NewGuid().ToString();

                    await using (var fileStream =
                                 new FileStream(Path.Combine(imagesFolderPath, imageKey + ".jpeg"), FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }

                    imageKeys.Add(imageKey);
                }
            }

            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();

                var values =
                    $"('{request.Name}', {request.Price}, '{request.Brand}', '{request.Description}', {(int)request.Category}, " +
                    $"{(int)request.SubCategory}, {request.Discount}, '{stockData}', {request.Novelty}, {request.Heat}, '{JsonConvert.SerializeObject(imageKeys)}', '{request.Composition}')";
                var command =
                    new MySqlCommand(
                        "INSERT INTO ShopItems(Name, Price, Brand, Description, Category, SubCategory, Discount, StockData, Novelty, Heat, Images, Composition) VALUES" +
                        values, connection);

                response.Success = command.ExecuteNonQuery() == 1;
            }

            return response;
        }

        [HttpPost("UpdateItem")]
        public async Task<ShopResponse> UpdateItem([FromForm] UpdateShopItemRequest request)
        {
            Program.ServerLogger.Log($"HttpPost | Shop/UpdateItem");
            Program.ServerLogger.Log(request);
            
            var response = new ShopResponse();
            if (string.IsNullOrEmpty(request.Name) || request.Price <= 0 || request.StockData == null
                || request.StockData.Count == 0)
            {
                response.ErrorMessage = "Wrong data";
                return response;
            }

            var stockDataList = request.StockData.Select(element => new StockData
            {
                Size = element.Size,
                Color = element.Color,
                Code = element.Code,
                Price = element.Price == "0" || !float.TryParse(element.Price, out var parsed) ? request.Price : parsed,
                Available = 1
            });

            var stockData = JsonConvert.SerializeObject(stockDataList);
            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();

                var command = new MySqlCommand(
                    $"UPDATE ShopItems SET Name='{request.Name}',Brand='{request.Brand}',Description='{request.Description}',Category={(int)request.Category}," +
                    $"SubCategory={(int)request.SubCategory},Price={request.Price},Discount={request.Discount},StockData='{stockData}'," +
                    $"Novelty={request.Novelty},Heat={request.Heat},Composition='{request.Composition}'" +
                    $" WHERE Id = {request.Id}",
                    connection);

                response.Success = command.ExecuteNonQuery() == 1;
            }

            return response;
        }

        [HttpDelete("DeleteItem")]
        public async Task<bool> Delete(int id)
        {
            Program.ServerLogger.Log($"HttpDelete | Shop/DeleteItem");
            Program.ServerLogger.Log($"id: {id}");
            
            bool result = false;
            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();

                var dataAdapter = new MySqlDataAdapter("SELECT * FROM ShopItems WHERE Id = " + id, connection);
                var dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                if (dataTable.Rows.Count > 0)
                {
                    var imagesString = (string)dataTable.Rows[0].ItemArray[11];

                    if (!string.IsNullOrEmpty(imagesString))
                    {
                        var images = JsonConvert.DeserializeObject<List<string>>(imagesString);

                        for (int i = 0; i < images.Count; i++)
                        {
                            var imagePath =
                                $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}/server/images/{images[i]}.jpeg";

                            if (System.IO.File.Exists(imagePath))
                            {
                                System.IO.File.Delete(imagePath);
                            }
                        }
                    }

                    var command = new MySqlCommand("DELETE FROM ShopItems WHERE Id = " + id, connection);
                    result = command.ExecuteNonQuery() == 1;
                }
            }

            return result;
        }

        [HttpDelete("RemoveImage")]
        public async Task<IActionResult> RemoveImage(string id, string imageKey)
        {
            Program.ServerLogger.Log($"HttpDelete | Shop/RemoveImage");
            Program.ServerLogger.Log($"id: {id}, imageKey: {imageKey}");
            
            var response = new RemoveImageResponse();

            if (string.IsNullOrEmpty(id))
                return Ok(response);

            await using (var connection = new MySqlConnection(Configs.ConnectStr))
            {
                await connection.OpenAsync();

                var dataAdapter = new MySqlDataAdapter("SELECT * FROM ShopItems WHERE Id = " + id, connection);
                var dataTable = new DataTable();
                dataAdapter.Fill(dataTable);

                if (dataTable.Rows.Count > 0)
                {
                    var imagesFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                                           "/server/images";

                    var imagesString = (string)dataTable.Rows[0].ItemArray[11];

                    if (!string.IsNullOrEmpty(imagesString))
                    {
                        var imagesPaths = JsonConvert.DeserializeObject<List<string>>(imagesString);
                        response.AllKeys = new List<string>(imagesPaths);

                        for (int i = 0; i < imagesPaths.Count; i++)
                        {
                            var imagePath = $"{imagesFolderPath}/{imagesPaths[i]}.jpeg";

                            if (imagesPaths[i] == imageKey && System.IO.File.Exists(imagePath))
                            {
                                System.IO.File.Delete(imagePath);

                                if (response.AllKeys.Contains(imagesPaths[i]))
                                    response.AllKeys.Remove(imagesPaths[i]);

                                break;
                            }
                        }
                    }
                }

                var command = new MySqlCommand(
                    $"UPDATE ShopItems SET Images='{JsonConvert.SerializeObject(response.AllKeys)}' WHERE Id = {id}",
                    connection);

                response.Success = command.ExecuteNonQuery() == 1;
            }

            return Ok(response);
        }
    }
}