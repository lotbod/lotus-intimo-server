using System;

namespace Server.Controllers.Data
{
    [Serializable]
    public class AreasResponseData
    {
        public AreaInfo[] data { get; set; }
            
        [Serializable]
        public class AreaInfo
        {
            public string Ref { get; set; }
            public string Description { get; set; }
        }
    }
}