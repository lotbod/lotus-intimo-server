using System;

namespace Server.Controllers.Data
{
    [Serializable]
    public class DepartmentsResponseData
    {
        public bool success { get; set; }
        public DepartmentInfo[] data { get; set; }
            
        [Serializable]
        public class DepartmentInfo
        {
            public string ShortAddress { get; set; }
            public string Description { get; set; }
        }
    }
}