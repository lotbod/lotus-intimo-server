using System;

namespace Server.Controllers.Data
{
    [Serializable]
    public class CitiesResponseData
    {
        public bool success { get; set; }
        public CityInfo[] data { get; set; }

        [Serializable]
        public class CityInfo
        {
            public string Description { get; set; }
            public string Ref { get; set; }
            public string Area { get; set; }
        }
    }
}